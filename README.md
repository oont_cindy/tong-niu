# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Accessing IRS 990 Filings and read the first 500 filings in 2015. 
* Extracting and plotting the density of Gross Receipt Amounts of the 500 filings.

### Code ###

```{r}
library(xml2)
library(jsonlite)
library(ggplot2)

data2015 <- fromJSON("https://s3.amazonaws.com/irs-form-990/index_2015.json")
url2015 <- data2015$Filings2015$URL


# Read first 500 filings in 2015
r <- lapply(url2015[1:500], read_xml)
list2015 <- lapply(r, xml_children)


# Get all Gross Receipt Amounts
gross.receipt <- function(l){
  tr <- l[2]
  tr_c <- xml_child(tr,1)
  xml_ns_strip(tr_c)
  gr <- xml_find_first(tr_c, ".//GrossReceiptsAmt")
  as.integer(xml_text(gr))
}

gross.receipt.amts <- lapply(list2015, gross.receipt)

gra <- unlist(gross.receipt.amts)
final.gra <- as.data.frame(gra[which(!is.na(gra) & gra != 0)])
names(final.gra) <- "gra"


# Plot the density of Gross Receipt Amounts
ggplot(final.gra, aes(gra)) +
  geom_density()


# Exclude larger value ( >= 75%)
larger <- quantile(gra,na.rm = T)[[4]]
final.gra.ex <- as.data.frame(gra[which(!is.na(gra) & gra != 0 & gra < larger)])
names(final.gra.ex) <- "gra"


# Plot the density of Gross Receipt Amounts
ggplot(final.gra.ex, aes(gra)) +
  geom_density()
```



### What to do in the future ###

* Investigate relationships among more features and build machine learning models for prediction
* Data visualization based on location and other features